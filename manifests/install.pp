class nvm::install {
  exec { 'install nvm':
    command     => "/usr/bin/wget -qO- https://raw.githubusercontent.com/creationix/nvm/${nvm::version}/install.sh | /bin/bash",
    environment => ["NVM_DIR=$nvm::install_dir"],
  }->
  file { '/etc/profile.d/nvm.sh':
    ensure  => present,
    content => epp('nvm/nvm.sh.epp'),
  }

  $nvm::node_versions.each |$node_version| {
    nvm::download_node { $node_version:
    }
  }

  exec { 'set default node version in nvm':
    command => "/bin/bash -l -c 'nvm alias default ${nvm::default_node_version}'",
    require => Nvm::Download_node[$nvm::default_node_version],
  }
}
